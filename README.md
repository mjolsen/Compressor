# comp.dsp

comp.dsp is a collection of FAUST (
[here](faust.grame.fr)) objects that work together to 
implement feedforward and feedback compressor architectures. A variety of
different types of level detectors are included including detectors that
release to threshold, release to value and implement program dependency.
The implementation is closely based on the ideas taught in the Stanford
University course MUSIC 424: Signal Processing Techniques for Digital
Audio Effects.

---

To use this code, you need to either have FaustLive or the Faust compiler 
installed on your system (
[here](http://faust.grame.fr/download/)) or use the online 
Faust Compiler (
[here](http://faust.grame.fr/onlinecompiler/)).

---

This code is licensed with the Synthesis Tool Kit 4.3 license an MIT-style 
license (see LICENSE)

---

Comments, questions and suggestions can be directed to 
mjolsen@ccrma.stanford.edu
